﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ProductionCode;

namespace TestProductionCode
{
    [TestClass]
    public class CheckConsumptionRuleTest
    {

        [ExpectedException(typeof(NotImplementedException))]
        [TestMethod]
        public void TestDefaultCase()
        {
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            object[] args = { 0, 0 };
            ruleChecker.ValidateRule(readingValue, null, args);

            //Assert.AreEqual(1,1);
            //var ruleMock = new Mock<IRule>(); //https://github.com/Moq/moq4/wiki/Quickstart
        }

        //cazul HCA_WITHOUT_KEYDATE al switch-ului
        [TestMethod]
        public void TestMethod2()
        {
            //nu intra in if-ul mare
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();

            readingValue.ReadingMask = AppConstants.HCA_WITHOUT_KEYDATE;
            object[] args = { 0, -1 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);

            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod3()
        {
            //intra in if-ul mare, dar nu intra in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HCA_WITHOUT_KEYDATE;
            object[] args = { 10, 800 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);

            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod4()
        {
            //intra in if-ul mare si in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HCA_WITHOUT_KEYDATE;
            object[] args = { 300, 1000 };
            readingValue.DueDateValue = 10;
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            Assert.IsTrue(result.Errors.Contains("MS3_ManualReadingValues_ValueDifferesTooMuch"));
            Assert.IsTrue(result.IsError);
        }

        //cazul HCA_WITH_KEYDATE al switch-ului
        [TestMethod]
        public void TestMethod5()
        {
            //nu intra in if-ul mare
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HCA_WITH_KEYDATE;
            object[] args = { 1, -100 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);

            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod6()
        {
            //intra in if-ul mare, dar nu intra in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HCA_WITH_KEYDATE;
            object[] args = { 20, 700 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            readingValue.DueDateValue = 5;
            readingValue.DueDateValueCache = 5;
            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod7()
        {
            //intra in if-ul mare si in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HCA_WITH_KEYDATE;
            object[] args = { 500, 1100 };
            readingValue.DueDateValue = 100;
            readingValue.DueDateValueCache = 40;
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            Assert.IsTrue(result.Errors.Contains("MS3_ManualReadingValues_ValueDifferesTooMuch"));
            Assert.IsTrue(result.IsError);
        }

        /*cazul WATER_METER/WATER_METER_WITH_DATAMODULE ale switch-ului. Cum WATER_METER e gol si nu prezinta
          instructiune de break, switch-ul va cascada spre cazul WATER_METER_WITH_DATAMODULE*/
        [TestMethod]
        public void TestMethod8()
        {
            //nu intra in if-ul mare
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.WATER_METER;
            object[] args = { -3, -2 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);

            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod9()
        {
            //intra in if-ul mare, dar nu intra in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.WATER_METER;
            object[] args = { 104, 630 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            readingValue.DueDateValue = 5;
            readingValue.DueDateValueCache = 5;
            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod10()
        {
            //intra in if-ul mare si in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.WATER_METER;
            object[] args = { 900, 1600 };
            readingValue.DueDateValue = 250;
            readingValue.DueDateValueCache = 70;
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            Assert.IsTrue(result.Errors.Contains("MS3_ManualReadingValues_ValueDifferesTooMuch"));
            Assert.IsTrue(result.IsError);
        }

        //cazul HEAT_METER

        [ExpectedException(typeof(InvalidOperationException))]
        [TestMethod]
        public void TestMethod11()
        {
            //intra in if-ul mare si in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HEAT_METER;
            object[] args = { 0, 1 };
            ruleChecker.ValidateRule(readingValue, null, args);
        }

        [TestMethod]
        public void TestMethod12()
        {
            //nu intra in if-ul mare
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HEAT_METER;
            object[] args = { -3, -2 , 1, 2};
            var  mock = new Mock<IConverter>();
            mock.Setup(converter => converter.Convert(args[2], args[3], 7.6)).Returns(9.8);
            var result = ruleChecker.ValidateRule(readingValue, mock.Object, args);
          
            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod13()
        {
            //intra in if-ul mare, dar nu intra in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HEAT_METER;
            object[] args = { 104, 630 };
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            readingValue.DueDateValue = 5;
            readingValue.DueDateValueCache = 5;
            Assert.IsNull(result.Errors);
            Assert.IsFalse(result.IsError);
        }

        [TestMethod]
        public void TestMethod14()
        {
            //intra in if-ul mare si in cel de AreTrialsAvailable
            var ruleChecker = new CheckConsumptionRule();
            var readingValue = new DeviceReadingValueDto();
            readingValue.ReadingMask = AppConstants.HEAT_METER;
            object[] args = { 900, 1600 };
            readingValue.DueDateValue = 250;
            readingValue.DueDateValueCache = 70;
            var result = ruleChecker.ValidateRule(readingValue, null, args);
            Assert.IsTrue(result.Errors.Contains("MS3_ManualReadingValues_ValueDifferesTooMuch"));
            Assert.IsTrue(result.IsError);
        }
    }
}
