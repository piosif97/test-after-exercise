﻿namespace ProductionCode
{
    public interface IRule
    {
        ValidationResult ValidateRule(DeviceReadingValueDto deviceReadingValueDto, IConverter converter, params object[] args);
    }
}
