﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionCode
{
    public interface IConverter
    {
        double Convert(object from, object to, double value);
    }
}
