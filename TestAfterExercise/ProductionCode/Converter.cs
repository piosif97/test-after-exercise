﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionCode
{
    public class Converter : IConverter
    {
        public double Convert(object from, object to, double value)
        {
            return ValidationHelper.ConvertValue(from, to, value);
        }
    }
}
